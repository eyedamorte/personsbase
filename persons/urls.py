from django.urls import path
from django.conf.urls import url, include
from . import views


urlpatterns = [
    path('/', views.plist, name='plist'),
    path('/edit/', views.edit, name='edit'),
    path('/add/', views.add, name='add'),
    path('/del/', views.delt, name='del'),
    path('/servicedel/', views.serviceDel, name='sdel'),
]
