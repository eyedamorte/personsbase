from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from authu.models import Division
from .models import Person, Service, Rank

# Create your views here.

def add(request):

    if request.method == 'POST':
        id = request.session['member_id']
        frame = Division.objects.get(id__exact=id)
        srvc = Service()
        srvc.name = request.POST['firstname']
        srvc.cerviced = frame
        srvc.save()


    return HttpResponseRedirect("/persons/")


def delt(request):

    if request.method == 'POST':
        dl = request.POST['delete']
        Service.objects.filter(id=dl).delete()

    return HttpResponseRedirect("/persons/")



def plist(request):



    #вывести Service онли те, которые у данного подразделения
    try:
        if request.session['member_id']:
            hentai = request.session['member_id']
            nonedata = Person.objects.filter(service_id=None)
            frame = Division.objects.get(id__exact=hentai)
            service_frame = Service.objects.filter(cerviced_id=hentai)
            ranks = Rank.objects.all()
            srvc = Service.objects.all()

            if request.method == 'POST':
                req_s = request.POST['select']
                s_id = Service.objects.get(id__exact=req_s)
                person = Person()
                person.firstname = request.POST['name']
                person.phone = request.POST['phone']
                person.shorty_num = request.POST['sh_phone']
                person.another_num= request.POST['another_phone']
                person.sq = request.POST['sq']
                person.email = request.POST['email']
                person.service = s_id
                person.division = frame
                person.position = request.POST['pos']
                person.rank = Rank.objects.get(id__exact=request.POST['rank'])
                person.save()
                # выводить только те сервисы, что добавил этот человек

            return render(request, 'place.html',locals())
    except:
        return HttpResponseRedirect("/")



def edit(request):
    try:
        if request.method == 'POST':

            if request.POST['edit']:
                edt= request.POST['edit']
                person = Person.objects.get(id=edt)
                try:
                    qrank = Rank.objects.get(id__exact=request.POST['rank'])
                except:
                    qrank = person.rank
                try:

                    person.firstname = request.POST['name']
                    person.phone = request.POST['phone']
                    person.shorty_num = request.POST['sh_phone']
                    person.another_num = request.POST['another_num']
                    person.sq = request.POST['sq']
                    person.email = request.POST['email']
                    person.position = request.POST['pos']
                    person.rank = qrank
                    person.save()
                except:
                    pass
    except:
        pass

        if request.POST['delete']:
            dl = request.POST['delete']
            Person.objects.filter(id=dl).delete()



    return HttpResponseRedirect("/persons/")

def serviceDel(request):
    if request.method == 'POST':
        serviceid = request.POST['ser']
        try:
            verif = request.POST['verif']

            if  verif == "True":
                Service.objects.filter(id=serviceid).delete()
            else:
                err = "подтвердите удаление"
        except:
            pass

    return HttpResponseRedirect("/persons/")
