from django.contrib import admin
from .models import Person, Service, Rank


# Register your models here.
class PersonAdmin(admin.ModelAdmin):
    class Meta:
        model = Person

admin.site.register(Person, PersonAdmin)

class ServiceAdmin(admin.ModelAdmin):
    class Meta:
        model = Service

admin.site.register(Service, ServiceAdmin)

class RankAdmin(admin.ModelAdmin):
    class Meta:
        model = Rank

admin.site.register(Rank, RankAdmin)
