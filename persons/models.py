from django.db import models
from authu.models import Division
# Create your models here.

class Service(models.Model):
        name = models.TextField( max_length=256, blank=True, null=True, default=None)
        cerviced = models.ForeignKey(Division, blank=True, null=True, default=None, related_name='serviced', on_delete=models.CASCADE)
        created = models.DateTimeField(auto_now_add=True, auto_now=False)
        updated = models.DateTimeField(auto_now_add=False, auto_now=True)
        class Meta:
            verbose_name = 'служба'
            verbose_name_plural = 'службы'

        def __str__(self):
            return str(self.name)


class Rank(models.Model):
        name = models.CharField( max_length=256, blank=True, null=True, default=None)
        created = models.DateTimeField(auto_now_add=True, auto_now=False)
        updated = models.DateTimeField(auto_now_add=False, auto_now=True)
        class Meta:
            verbose_name = 'звание'
            verbose_name_plural = 'звания'

        def __str__(self):
            return str(self.name)



class Person(models.Model):
        firstname = models.CharField( max_length=64, blank=True, null=True, default=None)
        sq = models.CharField( max_length=64, blank=True, null=True, default=None)
        rank = models.ForeignKey(Rank, blank=True, null=True, default=None, related_name='rank', on_delete=models.CASCADE)
        email = models.EmailField( max_length=64, blank=True, null=True, default=None)
        position = models.CharField( max_length=64, blank=True, null=True, default=None)
        shorty_num = models.CharField( max_length=64, blank=True, null=True, default=None)
        division = models.ForeignKey(Division, blank=True, null=True, default=None, related_name='persond', on_delete=models.CASCADE)
        service = models.ForeignKey(Service, blank=True,  null=True,  default=None, related_name='persons', on_delete=models.CASCADE)
        phone = models.CharField( max_length=256, blank=True, null=True, default=None)
        created = models.DateTimeField(auto_now_add=True, auto_now=False)
        updated = models.DateTimeField(auto_now_add=False, auto_now=True)
        another_num = models.CharField( max_length=264, blank=True, null=True, default=None)

        class Meta:
            verbose_name = 'человек'
            verbose_name_plural = 'люди'

        def __str__(self):
            return str(self.firstname)
