from django import template

register = template.Library()

@register.filter
def to_breake(value):
    try:
        return value.replace(" ", ', \n')
    except:
        pass
