from rest_framework import viewsets
from rest_framework.routers import DefaultRouter
from api.authu.views import DivisionViewSet, GroupViewSet

router = DefaultRouter()

router.register(r'divisions', DivisionViewSet, basename='divisions')
router.register(r'groups', GroupViewSet, basename='groups')

urlpatterns = router.urls
