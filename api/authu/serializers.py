from rest_framework import serializers
from authu.models import Group, Division

class DivisionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Division
        fields = ('id', 'name')

class GroupSerializer(serializers.ModelSerializer):
    divisions = DivisionSerializer(many=True)
    class Meta:
        model = Group
        fields = ('id', 'name', 'divisions')
