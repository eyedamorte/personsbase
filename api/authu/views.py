from rest_framework import viewsets
from authu.models import Division, Group
from .serializers import DivisionSerializer, GroupSerializer

class DivisionViewSet(viewsets.ModelViewSet):

    serializer_class = DivisionSerializer
    queryset = Division.objects.all()

class GroupViewSet(viewsets.ModelViewSet):

    serializer_class = GroupSerializer
    queryset = Group.objects.all()
