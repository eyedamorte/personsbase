from .models import Division, Group
from django.contrib import admin

class DivisionAdmin(admin.ModelAdmin):
    class Meta:
        model = Division

admin.site.register(Division, DivisionAdmin)

class GroupAdmin(admin.ModelAdmin):
    class Meta:
        model = Group

admin.site.register(Group, GroupAdmin)
