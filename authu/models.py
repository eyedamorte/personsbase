from django.db import models



class Group(models.Model):
        name = models.CharField( max_length=256, blank=True, null=True, default=None)
        created = models.DateTimeField(auto_now_add=True, auto_now=False)
        updated = models.DateTimeField(auto_now_add=False, auto_now=True)


        class Meta:
            verbose_name = 'Группа'
            verbose_name_plural = 'Группы'


        def __str__(self):
            return str(self.name)


class Division(models.Model):
        name = models.CharField( max_length=256, blank=True, null=True, default=None)
        login = models.CharField( max_length=64, blank=True, null=True, default=None)
        password = models.CharField( max_length=64, blank=True, null=True, default=None)
        created = models.DateTimeField(auto_now_add=True, auto_now=False)
        updated = models.DateTimeField(auto_now_add=False, auto_now=True)
        group = models.ForeignKey('Group', blank=True, null=True, default=None, related_name='div',  on_delete=models.CASCADE)

        def save(self, *args, **kwargs):
            self.name = self.name.lower()
            return super(Division, self).save(*args, **kwargs)

        class Meta:
            verbose_name = 'подразделение'
            verbose_name_plural = 'подразделения'
            unique_together = ['group', 'name']
            ordering = ['group']

        def __str__(self):
            return '%d: %s' % (self.group, self.name)
