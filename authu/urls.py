from django.urls import path
from django.conf.urls import url, include
from . import views


urlpatterns = [
    path('', views.table, name='table'),
    path('login', views.login, name='login'),
    path('logout', views.logout, name='logout'),
    path('persons',include('persons.urls') , name='add'),
    path('search/', views.table, name='search'),
]
